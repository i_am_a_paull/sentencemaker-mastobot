package us.oliverpaull.sentenceMaker.demo.mastodon;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.google.gson.Gson;
import com.sys1yagi.mastodon4j.MastodonClient;
import com.sys1yagi.mastodon4j.MastodonRequest;
import com.sys1yagi.mastodon4j.api.entity.Status;
import com.sys1yagi.mastodon4j.api.entity.Status.Visibility;
import com.sys1yagi.mastodon4j.api.exception.Mastodon4jRequestException;
import com.sys1yagi.mastodon4j.api.method.Statuses;

import okhttp3.OkHttpClient;
import us.oliverpaull.sentenceMaker.demo.engines.StatusEngine;

public class SMBot {

  private MastodonClient client;
  private StatusEngine engine;
  private int minSentences;
  private int maxSentences;

  public SMBot() throws IOException {
    Properties props = new Properties();
    props.load(new FileInputStream(new File("bot-settings.properties")));

    minSentences = Integer.parseInt(props.getProperty("min-sentences", "1"));
    maxSentences = Integer.parseInt(props.getProperty("max-sentences", "3"));

    client =
        new MastodonClient.Builder(
                props.getProperty("instance-url"), new OkHttpClient.Builder(), new Gson())
            .accessToken(props.getProperty("access-token"))
            .build();

    engine = new StatusEngine(props.getProperty("dictionary-loc"));
  }

  public void toot() throws Mastodon4jRequestException {
    String status = engine.getStatus(minSentences, maxSentences);

    System.out.println("sending toot: " + status);

    Statuses statuses = new Statuses(client);
    MastodonRequest<Status> req =
        statuses.postStatus(
            status, null, null, false, "bot-generated text, probably lewd", Visibility.Unlisted);
    req.execute();
  }

  public static void main(String... args) {
    try {
      SMBot bot = new SMBot();
      bot.toot();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
