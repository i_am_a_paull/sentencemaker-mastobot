package us.oliverpaull.sentenceMaker.demo.engines;

import java.util.List;

import com.google.common.collect.Lists;

import us.oliverpaull.sentenceMaker.engine.SMEngine;

public class StatusEngine extends SMEngine {

  public StatusEngine(String dbLocation) {
    super(dbLocation);
  }

  public String getStatus(int minSentences, int maxSentences) {
    int numSentences = minSentences + (int) (Math.random() * ((maxSentences - minSentences) + 0.9));
    List<String> sentences = Lists.newArrayList();
    for (int i = 0; i < numSentences; i++) {
      sentences.add(getSentence(1 + (int) (Math.random() * 2.9)));
    }
    return String.join(" ", sentences.toArray(new String[0]));
  }
}
